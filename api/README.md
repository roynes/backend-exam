## Auth, Post, and Comment API

# Installation instructions

```
composer install

php artisan jwt:secret
```

# Database migration
```
php artisan migrate
```
