<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'parent_id', 'body', 'creator_id'
    ];

    public function commentable()
    {
        return $this->morphTo();
    }
}
