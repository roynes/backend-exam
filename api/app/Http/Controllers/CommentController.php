<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest as Request;
use App\Comment;
Use App\Post;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ["except" => ['index', 'show']]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Post $post)
    {
        $comment = $post->comments()
            ->create(
                array_merge($request->all(), [
                    "creator_id" => auth()->user()->id
                ])
            );

        return response()
            ->json(['data' => $comment])
            ->setStatusCode(201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post, Comment $comment)
    {
        $parent = $post->hasComment($comment->id);

        if(!$parent)
            return response()
                ->json(['status' => 'record not found']);

        $comment->update($request->all());

        return response()->json(['data' => $comment]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post, Comment $comment)
    {
        $parent = $post->hasComment($comment->id);

        if(!$parent)
            return response()
                ->json(['status' => 'record not found']);

        $comment->delete();

        return response()->json(['status' => 'record deleted successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post, Comment $comment)
    {
        $parent = $post->hasComment($comment->id);

        if(!$parent)
            return response()
                ->json(['status' => 'record not found']);

        return response()->json(['data' => $comment]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Post $post)
    {
        return response()->json([
            'data' => $post->comments()
                ->latest()
                ->paginate(10)
                ->all()
        ]);
    }
}
