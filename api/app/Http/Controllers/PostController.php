<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostsRequest as Request;
use Illuminate\Support\Str;
use App\Post;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ["except" => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Post $post)
    {
        return response()->json(["data" => $post->latest()->paginate(10)->all()]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Post $post)
    {
        $data = $request->all();

        $createdPost = auth()->user()->post()->create(
            array_merge(
                $data,
                [ "slug" => Str::kebab($data['title'])]
            )
        );

        return response()->json(['data' => $createdPost])->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()
            ->json(['data' => Post::whereSlug($id)->get()]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);

        if(!$post)
            return response()->json(['status' => 'record not found']);

        $post->update($request->all());

        return response()->json(['data' => $post]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        if(!$post)
            return response()->json(['status' => 'record not found']);

        $post->delete();

        return response()->json([
            'status' => 'record deleted successfully'
        ]);
    }
}
