<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(request()->method() === 'POST')
            return [
                "title" => "required|string|min:4",
                "content" => "required|string|min:4",
                "image" => "required|string|min:4"
            ];

        return [
            "title" => "required|string|min:4",
        ];
    }
}
