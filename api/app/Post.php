<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title', 'image', 'content', 'slug'
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function hasComment($commentId)
    {
        return $this->whereId($this->id)->whereHas(
            'comments',
            function ($query) use ($commentId) {
                $query->whereId($commentId);
            }
        )->get();
    }
}
